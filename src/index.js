const app = document.querySelector("#app");
const input = document.querySelector("#addTask");
const header = document.querySelector("header");
const tasksList = document.querySelector("#tasksList");

input.addEventListener('keypress', function (e) {
    const key = e.which || e.keyCode;
    if (key === 13) {
        const task = document.createElement('li');
        task.setAttribute("class", "task"),
        task.textContent = input.value;
        tasksList.appendChild(task);

        header.setAttribute("class", "toggleAll");

    }
});